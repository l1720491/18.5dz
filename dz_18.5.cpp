#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

//Create my own stack class.
template <class T> // �������� �������
class Stack     // ����� ����
{
private:
    //Encapsulated vars.
    T* Arr = NULL;   // ������ ������
    int size = 0;
    int top = 0;

public:

    Stack()
    {

    }

    ~Stack()            // ���������� ������ ����
    {
        delete[] Arr; // �������� ������ �������
    }

    void DynamicArrayLogic()
    {
        Type* T = new Type[size];   // ������
        //Copy values to temp array
        for (int i = 0; i < size; i++)
        {
            T[i] = Arr[i];
        }
        delete[] Arr;// Free old array memory
        Arr = T;//Now Arr Pointer to new/temp array.
    }

    //Add an element
    void push(T value)
    {
        Type* NewArr = new Type[size + 1];
        cout << "Add Element " << value << endl;

        for (int i = 0; i < size; i++)
        {
            NewArr[i] = Arr[i];
        }
        NewArr[size] = value;
        delete[] Arr;
        size++;
        Array = NewArr;
    }

    //remove top element
    void pop()
    {
        --size;
    }

    //������ ����� 
    void Print()
    {
        for (int i = 0; i < size; i++)
        {
            std::cout << *(Arr + i) << " ";
        }
        std::cout << std::endl;
    }


};// ����� ������� ����

int main()
{
    Stack<int> IntStack;
    Stack<string> StringStack;

    IntStack.push(1);
    IntStack.push(2);
    IntStack.push(100);
    IntStack.push(5);
    IntStack.push(4);
    /*}*/
    IntStack.Print();
    IntStack.pop();
    IntStack.Print();
    IntStack.pop();
    IntStack.Print();
    IntStack.pop();
    IntStack.Print();
    IntStack.push(100);
    IntStack.Print();

    cout << "----------------" << endl;

    StringStack.push("Test:0");
    StringStack.push("Test:1");
    StringStack.push("Test:2");
    StringStack.push("Test:3");
    StringStack.push("Test:4");
    StringStack.pop();
    StringStack.Print();
    StringStack.pop();
    StringStack.Print();
    StringStack.push("itsWork:1");
    StringStack.Print();
}